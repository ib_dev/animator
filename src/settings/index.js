/**
 * @module settings/
 */

import settings from './validation';

settings.animation.enabled = true;
settings.animation.remove = 'fade-out';
settings.animation.add = 'fade-in';
settings.animation.reorderDuration = 1000;
settings.animation.order = 'ard';
export default settings;
