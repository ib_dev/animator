/**
 * @module settings/validation
 */

import Verify from '../verify';


/**
 * Array consisting of strings representing animations type order.
 * @var {object} _orders
 */
const _orders = ['dra', 'dar', 'rda', 'rad', 'adr', 'ard'];

/**
 * Check if argument is valid HTML class attribute name.
 * @function _isClass
 * @param {string} val - HTML DOM class attribute name.
 */
const _isClass = val => /^[A-Za-z][-A-Za-z0-9_]*$/.test(val);

/**
 * Validates animation settings.
 * @function _validation
 * @param {string} fieldName - Validation method name.
 * @param {string|number|boolean} value - Validation method value.
 */
const _validation = (fieldName, value) => {
  switch (fieldName) {
    case 'enabled':
      if (typeof value !== 'boolean') throw new TypeError('Invalid value. Must be boolean');
      break;
    case 'reorderDuration':
      if (typeof value !== 'number' || value < 0) throw new Error('Invalid value. Must be number and > 0');
      break;
    case 'order':
      if (typeof value !== 'string' || !_orders.includes(value.toLowerCase())) {
        throw new Error('Invalid value. Must be string with value as dra, dar, rda, rar, adr or ard');
      }
      break;
    case 'remove':
    case 'add':
      if (value === '' || typeof value !== 'string' || !_isClass(value)) {
        throw new Error(`Invalid value. Must be string consisting at least one star. Must begin with a letter A-Z or a-z. Can be followed by: letters (A-Za-z), digits (0-9), hyphens ("-"), and underscores ("_").`);
      }
      break;
    default:
      throw new Error('Invalid property name. Valid names are enabled, remove, add, reorderDuration and order');
  }
  return true;
}

const settings = {animation: new Verify(['enabled', 'remove', 'add', 'reorderDuration', 'order'], _validation)};

export default settings;
