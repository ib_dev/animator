/**
 * @module h
 */


 /**
  * @function _isTextNode
  * @param {string|number|object} node - Virtual DOM node.
  */

const _isTextNode = node => typeof node === 'string'|| typeof node === 'number';


/**
 * Creating virtual DOM tree.
 * @function h
 * @param {string|boolean} type - Virtual node type.
 * @param {object} props - Virtual node properties. Information about DOM element attributes and events.
 * @param {string|number|object} children - Information about the virtual DOM node children node(s).
 * Multible children nodes if parameter type is object and one node if parameter type is string or number.
 */

const h = (type = '', props = {}, children = []) => {
  if (typeof type === 'boolean') return false;
  if (_isTextNode(children)) {
    children = [{type: 'textNode', content: children}];
  } else {
    const nodes = [];
    children.forEach(child => {
      if (_isTextNode(child)) {
        nodes.push({type: 'textNode', content: child});
      } else if (typeof child !== 'boolean') {
        nodes.push(child);
      }
    });
    return { type, props, children: nodes };
  }
  return { type, props, children };
}

export default h;
