import setProp from '../dom/setProp';
import rmProp from '../dom/rmProp';

/**
 * @module vdom/diffProps
 */

/**
 * Find differences between two objects property props.
 * When differences represent change, apply change from one object to another.
 * @function diffProps
 * @param {object} oldVnode - Information about HTML DOM element node with its children nodes.
 * @param {object} newVnode - Information about HTML DOM element node with its children nodes.
 */
const diffProps = (oldVnode, newVnode) => { 
  if (!oldVnode || !newVnode || !oldVnode.props && !newVnode.props) return false;

  const nAttrs = Object.keys(newVnode.props);
  const oAttrs = Object.keys(oldVnode.props);

  for (let i = 0; i < nAttrs.length || i < oAttrs.length; i++) {
    if (i < nAttrs.length) {
      if (!(nAttrs[i] in oldVnode.props) || newVnode.props[nAttrs[i]] !== oldVnode.props[nAttrs[i]]) {
        setProp(oldVnode.el, nAttrs[i], newVnode.props[nAttrs[i]]);
      }
    }

    if (i < oAttrs.length) {
      if (!(oAttrs[i] in newVnode.props)) {
        rmProp(oldVnode.el, oAttrs[i]);
      }
    }
  }
};

export default diffProps;
