
/**
 * @module vdom/updateByKey/
 */

import opMap from './opMap';
import settings from '../../settings/';
import pr from '../processingCtrl';
import domApi from './../../dom/domApi';
import createElement from './../../dom/createElement';

/**
 * Compares old virtual node children nodes with new ones and applies changes to DOM.
 * @function updateByKey
 * @param {object} $parent - HTML DOM Element Object.
 * @param {object} newVNC - Array of new virtual node children nodes.
 * @param {object} oldVNC - Array of old virtual node children nodes.
 * @param {object|undefined} animation - Settings for animation from virtual DOM tree creating function.
 */
function updateByKey($parent, newVNC, oldVNC, animation){
  animation = animation || settings.animation;
  pr.isActive = true;

  const o = (animation.order || settings.animation.order).split('');
  opMap.get(o[0])($parent, newVNC, oldVNC, animation)
    .then(noAnim => noAnim ? noAnim : opMap.get(o[1])($parent, newVNC, oldVNC, animation, noAnim))
    .then(noAnim => noAnim ? noAnim : opMap.get(o[2])($parent, newVNC, oldVNC, animation, noAnim))
    .then(noAnim => {
      if (noAnim) {
        oldVNC.forEach(vNode => {
          if (domApi.isInParent($parent, vNode.el)) domApi.removeChild($parent, vNode.el)
        })
        newVNC.forEach(vNode => {
          domApi.appendChild($parent, createElement(vNode))
        })
      }
      pr.isActive = false;
      pr.check();
  });
}

export default updateByKey;
