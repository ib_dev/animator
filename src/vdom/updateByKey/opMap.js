import removeNodes from './removeNodes';
import addNodes from './addNodes';
import reorderNodes from './reorderNodes';


/**
 * @module vdom/updateByKey/opMap
 */

/**
 * Map consisting of functions for DOM manipulations.
 * @var {object} opMap
 */
const opMap = new Map();
opMap.set('d', removeNodes).set('r', reorderNodes).set('a', addNodes);

export default opMap;
