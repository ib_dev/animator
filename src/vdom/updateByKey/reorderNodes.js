/**
 * @module vdom/updateByKey/reorderNodes
 */

import settings from '../../settings/';
import Transition from './animations/transition';
import domApi from '../../dom/domApi';
import updateElement from '../updateElement';
import createElement from '../../dom/createElement';

/**
 * Indicates whether virtual nodes are of same type and have identical key.
 * @function _sameVnode
 * @param {object} oldVnode - Information about HTML DOM element node with its children nodes.
 * @param {object} newVnode - Information about HTML DOM element node with its children nodes.
 */
function _sameVnode(oldVnode, newVnode){
  if (oldVnode.props.key === newVnode.props.key && oldVnode.type === newVnode.type) return true;
}

/**
 * Creates position by key map.
 * @function _createPosByKeyMap
 * @param {object} children - Array of virtual nodes.
 * @param {number} beginPos - Beginning position.
 * @param {number} endPos - Ending position.
 */
function _createPosByKeyMap(children, beginPos, endPos) {
  const map = new Map();
  for (let i = beginPos; i <= endPos; ++i) {
    if (children[i] !== undefined) {
      map.set(children[i].props.key, i);
    }
  }
  return map;
}

/**
 * Removes old nodes from DOM if their type has changed.
 * @function _removeDoubleNodes
 * @param {object} doubleElRefs - Array of HTML DOM Element Objects.
 */
function _removeDoubleNodes(doubleElRefs){
  if (!doubleElRefs.length) return false;
  for (let ref of doubleElRefs) {
    ref.remove();
  }
}

/**
 * Compares old virtual node children nodes with new ones and relocates nodes in DOM if necessary.
 * @function reorderNodes
 * @param {object} $parent - HTML DOM Element Object.
 * @param {object} newVNC - Array of new virtual node children nodes.
 * @param {object} oldVNC - Array of old virtual node children nodes.
 * @param {object} animation - Settings for animation from virtual DOM tree creating function.
 * @param {boolean|undefined} noAnim - Indicates whether animation is enabled.
 */
function reorderNodes($parent, newVNC, oldVNC, animation, noAnim){
  const time = animation.reorderDuration !== undefined
    ? animation.reorderDuration : settings.animation.reorderDuration;
  noAnim = animation.enabled !== undefined
    ? !animation.enabled : !settings.animation.enabled;
  let reordered = false;

  return new Promise(resolve => {
    if (oldVNC.length <= 1) resolve(noAnim);
    let oldStartPos = 0, newStartPos = 0, animationCount = 0;
    let oldEndPos = oldVNC.length - 1;
    let oldStartVnode = oldVNC[0];
    let oldEndVnode = oldVNC[oldEndPos];
    let newEndPos = newVNC.length - 1;
    let newStartVnode = newVNC[0];
    let newEndVnode = newVNC[newEndPos];
    let posByOldKey = null;
    const doubleElRefs = [];

    while (oldStartPos <= oldEndPos && newStartPos <= newEndPos) {
      if (oldStartVnode.moved) {
        oldStartVnode = oldVNC[++oldStartPos];
      }
      else if (_sameVnode(oldStartVnode, newStartVnode)) {
        updateElement($parent, newStartVnode, oldStartVnode);
        oldStartVnode = oldVNC[++oldStartPos];
        newStartVnode = newVNC[++newStartPos];
      }
      else if (_sameVnode(oldEndVnode, newEndVnode)) {
        updateElement($parent, newEndVnode, oldEndVnode);
        oldEndVnode = oldVNC[--oldEndPos];
        newEndVnode = newVNC[--newEndPos];
      }
      else if (_sameVnode(oldEndVnode, newStartVnode)) {
        updateElement($parent, newStartVnode, oldEndVnode);
        const trans = new Transition(oldEndVnode.el, time, ++animationCount, noAnim);
        trans.start().then(transition => {
          if (transition.last) resolve(transition.cancel);
        });
        reordered = true;
        domApi.insertBefore($parent, oldEndVnode.el, oldStartVnode.el);
        oldEndVnode = oldVNC[--oldEndPos];
        newStartVnode = newVNC[++newStartPos];
      }
      else {
        if (posByOldKey === null) {
          posByOldKey = _createPosByKeyMap(oldVNC, oldStartPos, oldEndPos);
        }
        const posInOld = posByOldKey.get(newStartVnode.props.key);
        if (posInOld === undefined) {
          newStartVnode = newVNC[++newStartPos];
        } else {
          const elmToMove = oldVNC[posInOld];
          if (elmToMove.type !== newStartVnode.type) {
            const newStartEl = createElement(newStartVnode);
            const trans = new Transition(elmToMove.el, time, ++animationCount, noAnim, newStartEl);
            trans.start().then(transition => {
              if (transition.last) resolve(transition.cancel);
            });
            reordered = true;
            domApi.insertBefore($parent, newStartEl, oldStartVnode.el);
            doubleElRefs.push(elmToMove.el);
          } else {
            updateElement($parent, newStartVnode, elmToMove);
            const trans = new Transition(elmToMove.el, time, ++animationCount, noAnim);
            trans.start().then(transition => {
              if (transition.last) resolve(transition.cancel);
            });
            reordered = true;
            domApi.insertBefore($parent, elmToMove.el, oldStartVnode.el);
            elmToMove.moved = true;
          }
          newStartVnode = newVNC[++newStartPos];
        }
      }
    }
    if (newStartPos > newEndPos) _removeDoubleNodes(doubleElRefs);
    if (!reordered) resolve(noAnim);
  });
}

export default reorderNodes;
