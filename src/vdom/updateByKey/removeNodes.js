/**
 * @module vdom/updateByKey/removeNodes
 */

import settings from '../../settings/';
import Animation from './animations/animation';
import domApi from '../../dom/domApi';


/**
 * Compares old virtual node children nodes with new ones and removes nodes from DOM if necessary.
 * @function removeNodes
 * @param {object} $parent - HTML DOM Element Object.
 * @param {object} newVNC - Array of new virtual node children nodes.
 * @param {object} oldVNC - Array of old virtual node children nodes.
 * @param {object} animation - Settings for animation from virtual DOM tree creating function.
 * @param {boolean|undefined} noAnim - Indicates whether animation is enabled.
 */
function removeNodes($parent, newVNC, oldVNC, animation, noAnim) {
  const animationName = animation.remove || settings.animation.remove;
  noAnim = animation.enabled !== undefined
    ? !animation.enabled : !settings.animation.enabled;
  let animationCount = 0;

  return new Promise((resolve, reject) => {
    const newVNCmap = new Map();
    for (let i = 0; i < newVNC.length; i++) {
      newVNCmap.set(newVNC[i].props.key, newVNC[i]);
    }
    let j = 0;
    for (let i = 0; i < oldVNC.length; i++) {
      if (!newVNCmap.has(oldVNC[i].props.key)) {
        j--;
        const el = oldVNC[i].el;
        const anim = new Animation(animationName, el, ++animationCount, noAnim);
        anim.start().then(animation => {
          domApi.removeChild($parent, el);
          if (animation.last) resolve(animation.cancel);
        });
        oldVNC.splice(i, 1);
        --i;
      } else {
        j++;
        if (oldVNC.length === j) {
          resolve();
        }
      }
    }
  })
}

export default removeNodes;
