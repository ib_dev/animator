
/**
 * @module vdom/updateByKey/animations/animation
 */

import requestFrame from '../../requestFrame';
import anims from '../../runningAnimations';


/** Animates HTML element using CSS3 animations. */
class Animation {
  /**
   * @param {string} AnimationName
   * @param {object} El - HTML DOM Element Object.
   * @param {number} AnimationCount - Count of currently running CSS3 animations.
   * @param {boolean|undefined} NoAnim - Indicates whether animation is enabled.
   */
  constructor(AnimationName, El, AnimationCount, NoAnim) {
    this.name = AnimationName;
    this.el = El;
    this.animationCount = AnimationCount;
    this.noAnim = NoAnim;
    this.animStarted = false;
    this.resolve = null;
    this.nameActive = '';
    this.clean = this.clean.bind(this);
    this.animationstartListener = this.animationstartListener.bind(this);
    this.animationendListener = this.animationendListener.bind(this);
  }
  animationstartListener() {
    this.animStarted = true;
    anims.push({clean: this.clean});
  }
  animationendListener() {
    this.clean();
  }
  /**
   * @param {boolean|undefined} cancel
   */
  clean(cancel) {
    anims.shift();
    this.el.removeEventListener("animationstart", this.animationstartListener);
    this.el.removeEventListener("animationend", this.animationendListener);
    this.el.classList.remove(this.name, this.nameActive);
    this.counter(cancel);
  }
  /**
   * @param {boolean|undefined} cancel
   */
  counter(cancel) {
    if (this.animationCount) --this.animationCount;
    if (this.animationCount === 0) {
      this.resolve({last: true, cancel: cancel});
    } else if(this.animationCount > 0) {
      this.resolve({last: false});
    } else {
      this.resolve();
    }
  }
  /**
   * @returns {object} - A promise.
   */
  start() {
    return new Promise(Resolve => {
      this.resolve = Resolve;

      if (this.noAnim) {
        this.counter(true);
        return false;
      }

      this.el.classList.add(this.name);
      this.nameActive = this.name + '-active';

      requestFrame(() => {
        this.el.classList.add(this.nameActive);
        this.el.addEventListener("animationstart", this.animationstartListener);

        requestFrame(() => {
          requestFrame(() => {
            if (!this.animStarted) {
              this.clean(false);
            }
          });
        });

        this.el.addEventListener("animationend", this.animationendListener);
      });

    });
  }
}

export default Animation;
