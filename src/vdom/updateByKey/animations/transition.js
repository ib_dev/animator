/**
 * @module vdom/updateByKey/animations/transition
 */

import requestFrame from '../../requestFrame';
import anims from '../../runningAnimations';


/** Animates HTML element using CSS3 transitions. */
class Transition {

  /**
   * @param {object} El - HTML DOM Element Object.
   * @param {number} Time - CSS3 transition time in milliseconds.
   * @param {number} AnimationCount - Count of currently running CSS3 transitions.
   * @param {boolean|undefined} NoAnim - Indicates whether animation is enabled.
   * @param {object|undefined} NewEl - HTML DOM Element Object.
   */
  constructor(El, Time, AnimationCount, NoAnim, NewEl) {
    this.el = El;
    this.time = Time;
    this.animationCount = AnimationCount;
    this.noAnim = NoAnim;
    this.newEl = NewEl;
    this.oldBox = {};
    this.newBox = {};
    this.resolve = null;
    this.deltaX = 0;
    this.deltaY = 0;
    this.clean = this.clean.bind(this);
    this.transitionendListener = this.transitionendListener.bind(this);
  }
  transitionendListener() {
    this.clean();
  }
  /**
   * @param {boolean|undefined} cancel
   */
  clean(cancel){
    anims.shift();
    this.el.removeEventListener("transitionend", this.transitionendListener);
    this.el.style.transition = '';
    this.counter(cancel);
  }
  /**
   * @param {boolean|undefined} cancel
   */
  counter(cancel){
    if (this.animationCount) --this.animationCount;
    if (this.animationCount === 0) {
      this.resolve({last: true, cancel: cancel});
    } else if(this.animationCount > 0) {
      this.resolve({last: false});
    } else {
      this.resolve();
    }
  }
  /**
   * @returns {object} - A promise.
   */
  start(){
    this.oldBox = this.el.getBoundingClientRect();

    return new Promise(Resolve => {
      this.resolve = Resolve;

      if (!this.time || this.noAnim){
        this.counter(this.noAnim);
        return false;
      }

      requestFrame(() => {
        this.el = this.newEl || this.el;
        this.newBox  = this.el.getBoundingClientRect();
        this.deltaX = this.oldBox.left - this.newBox.left;
        this.deltaY = this.oldBox.top  - this.newBox.top;
        this.el.style.transform  = `translate(${this.deltaX}px, ${this.deltaY}px)`;
        this.el.style.transition = 'transform 0s';
        anims.push({clean: this.clean});
        requestFrame(() => {
          this.el.style.transform  = '';
          this.el.style.transition = `transform ${this.time}ms`;
          this.el.addEventListener("transitionend", this.transitionendListener);
        });
      });
    });
  }
}

export default Transition;
