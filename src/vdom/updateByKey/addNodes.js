/**
 * @module vdom/updateByKey/addNodes
 */

import settings from '../../settings/';
import Animation from './animations/animation';
import createElement from '../../dom/createElement';
import domApi from '../../dom/domApi';


/**
 * Compares old virtual node children nodes with new ones and adds new nodes to DOM if necessary.
 * @function addNodes
 * @param {object} $parent - HTML DOM Element Object.
 * @param {object} newVNC - Array of new virtual node children nodes.
 * @param {object} oldVNC - Array of old virtual node children nodes.
 * @param {object} animation - Settings for animation from virtual DOM tree creating function.
 * @param {boolean|undefined} noAnim - Indicates whether animation is enabled.
 */
function addNodes($parent, newVNC, oldVNC, animation, noAnim) {
  let nextEl = null,
      prevEl = null;
  let animationCount = 0;
  const animationName = animation.add || settings.animation.add;
  noAnim = animation.enabled !== undefined
    ? !animation.enabled : !settings.animation.enabled;
  let prevElOnly = false;

  const oldVNCmap = new Map();
  for (let i = 0; i < oldVNC.length; i++) {
    if (oldVNC[i]) {
      oldVNCmap.set(oldVNC[i].props.key, i);
    }
  }

  return new Promise(resolve => {
    let j = 0;
    for (let i = 0; i < newVNC.length; i++) {
      const newVnode = newVNC[i];
      if (!oldVNCmap.has(newVnode.props.key)){
        j--;
        const newEl = createElement(newVnode);
        if (newVNC[i+1]) nextEl = newVNC[i+1].el;
        if (newVNC[i-1]) prevEl = newVNC[i-1].el;
        if (i === 0 && !nextEl && !prevEl) {
          domApi.prependChild($parent, newEl);
          oldVNC.splice(i, 0, newVnode);
        } else if (nextEl && !prevElOnly) {
          domApi.insertBefore($parent, newEl, nextEl);
          oldVNC.splice(i, 0, newVnode);
        } else if (prevEl) {
          domApi.insertAfter($parent, newEl, prevEl);
          oldVNC.splice(i, 0, newVnode);
        } else {
          domApi.prependChild($parent, newEl);
          oldVNC.splice(0, 0, newVnode);
        }

        const anim = new Animation(animationName, newEl, ++animationCount, noAnim);
        anim.start().then(animation => {
          if (animation.last) resolve(animation.cancel); 
        });

        prevElOnly = false;
      } else {
        prevElOnly = true;
        j++;
        if (newVNC.length === j) {
          resolve();
        }
      }
    }
  });
}

export default addNodes;
