
/**
 * @module vdom/requestFrame
 */

/**
 * Allows to execute code on the next available screen repaint.
 * Fallback with setTimeout() for older browsers.
 * @function requestFrame
 */
const requestFrame = requestAnimationFrame || function(f){return setTimeout(f, 1000/60)} 

export default requestFrame;
