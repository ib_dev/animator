import domApi from '../dom/domApi';
import updateByKey from './updateByKey/';
import createElement from '../dom/createElement';
import diffProps from './diffProps';


/**
 * @module vdom/updateElement
 */

/**
 * Checks if at least one array item has property named key with valid value.
 * @function _hasKey
 * @param {object} arr - Array of virtual nodes.
 */
const _hasKey = (arr) => arr.some(item => 'props' in item && 'key' in item.props &&
  item.props.key && typeof item.props.key === 'string');


/**
 * Compares old virtual node with new one and applies changes to DOM.
 * @function updateElement
 * @param {object} $parent - HTML DOM Element Object.
 * @param {object|boolean|undefined} newVnode - Information about HTML DOM element node with its children nodes.
 * @param {object|undefined} oldVnode - Information about HTML DOM element node with its children nodes.
 */
const updateElement = ($parent, newVnode, oldVnode) => {
  if (!newVnode && !oldVnode) return false; // nothing to update

  if (!oldVnode && newVnode) {
    domApi.appendChild($parent, createElement(newVnode));
  } else if (!newVnode) {
    domApi.removeChild($parent, oldVnode.el);
  } else if (newVnode.type !== oldVnode.type) {
    domApi.replaceChild($parent, createElement(newVnode), oldVnode.el);
  } else if (oldVnode.type === 'textNode' && newVnode.type === 'textNode') {
    if (newVnode.content !== oldVnode.content) {
      domApi.replaceChild($parent, createElement(newVnode), oldVnode.el);
    } else {
      newVnode.el = oldVnode.el;
    }
  } else {
    diffProps(oldVnode, newVnode);
    newVnode.el = oldVnode.el;

    if(_hasKey(newVnode.children) && _hasKey(oldVnode.children)){
      updateByKey(
        oldVnode.el,
        newVnode.children,
        oldVnode.children,
        newVnode.props.animation
      );
    } else {
      const max = Math.max(newVnode.children.length, oldVnode.children.length);
      for (let i = 0; i < max; i++) {
        updateElement(
          oldVnode.el,
          newVnode.children[i],
          oldVnode.children[i]
        );
      }

    }
  }
  return newVnode;
}

export default updateElement;
