
/**
 * @namespace pr
 */

/**
 * @memberof processLauncher
 * @property {object}  pendingProcesses - List of pending processes.
 */

/**
 * @memberof processLauncher
 * @property {boolean}  processActive - Information about process state.
 */

/**
 * Verifies readiness for lounching new process and then executes this process based on that condition
 * if there are pending new processes.
 * @memberof domApi
 * @function check
 */

const pr = {
  pending: [],
  isActive: false,
  check(){
    if (!this.isActive && this.pending.length){
      const pp = this.pending[0];
      pp.resolve(pp.updateElement(pp.root, ...pp.args));
      this.pending.splice(0, 1);
    }
  }
};

export default pr;
