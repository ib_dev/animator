/**
 * @module vdom/runningAnimations
 */

/**
 * Array for methods for canceling running CSS animations or transitions.
 */
export default [];
