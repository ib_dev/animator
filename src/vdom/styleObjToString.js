/**
 * @module vdom/styleObjToString
 */

/**
 * Converts object to string representing CSS styles.
 * @function styleObjToString
 * @param {object} props - Describes CSS styles.
 */
const styleObjToString = props => {
  const attrs = Object.keys(props);
  return attrs.reduce((sum, value) => sum + value.replace(/([A-Z])/g, '-$1').toLowerCase() + ':' + props[value] + ';', '');
};

export default styleObjToString;
