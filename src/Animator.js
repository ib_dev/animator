import updateElement from './vdom/updateElement';
import anims from './vdom/runningAnimations';
import pr from './vdom/processingCtrl';

class Animator {
  constructor(root) {
    this.root = root;
    this.oldView = null;
  }
  cancelAnimation() {
    return new Promise((resolve, reject) => {
      if (anims.length === 0){
        resolve({noAnims: true})
        return false;
      }
      for (let i = 0; i < anims.length; i++) {
        const obj = anims[i];
        obj.clean(true);
        anims.splice(i, 1);
        --i;
        if (anims.length === 0) resolve({noAnims: false});
      }
    });
  }
  set render(newView) {
    this.process(newView, this.oldView).then(view => {
      this.oldView = view;
    });
  }
  process(...args) {
    return new Promise((resolve, reject) => {
      this.cancelAnimation().then( res => {
        pr.pending.push({
          resolve,
          updateElement,
          root: this.root,
          args: [...args]
        });
        if (pr.pending.length > 1) {
          pr.pending.shift();
        }
        if (res.noAnims) pr.check();
      });
    });
  }
}

export default Animator;
