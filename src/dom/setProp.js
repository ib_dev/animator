import addEvent from './addEvent';
import styleObjToString from '../vdom/styleObjToString';

/**
 * @module dom/setProp
 */

/**
 * Add attribute or event listener to HTML DOM element node.
 * @function setProp
 * @param {object} $el - HTML DOM Element Object.
 * @param {string} name - HTML DOM Attribute or event name.
 * @param {any} value - HTML DOM Attribute or event value.
 */
const setProp = ($el, name, value) => {
  if (name === 'animation' && typeof value !== 'string') return false;
  if (name === 'className') {
    $el.setAttribute('class', value);
  } else if (name === 'style' && typeof value !== 'string') {
    $el.setAttribute('style', styleObjToString(value));
  } else if (/^on/.test(name)) {
    addEvent($el, name, value);
  } else {
    $el.setAttribute(name, value);
  }
};

export default setProp;
