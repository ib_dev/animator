/**
 * @module dom/domApi
 */

/**
 * Api for dom manipulations.
 * @namespace domApi
 */


/**
 * Inserts the specified node before the reference node as a child of the current node.
 * @function insertBefore
 * @memberof domApi
 * @param {object} parentNode - HTML DOM Element Object.
 * @param {object} newNode - HTML DOM Element Object.
 * @param {object|null} referenceNode - HTML DOM Element Object or null.
 */

/**
 * Returns the node immediately following the specified one in its parent's childNodes list, or null if the specified node is the last node in that list.
 * @function nextSibling
 * @memberof domApi
 * @param {object} node - HTML DOM Element Object.
 */

 /**
  * Checks if parentNode contains node.
  * @function isInParent
  * @memberof domApi
  * @param {object} parentNode - HTML DOM Element Object.
  * @param {object} node - HTML DOM Element Object.
  */

/**
 * Insert node after the reference node.
 * @function insertAfter
 * @memberof domApi
 * @param {object} parentNode - HTML DOM Element Object.
 * @param {object} newNode - HTML DOM Element Object.
 * @param {object} referenceNode - HTML DOM Element Object.
 */

/**
 * Inserts node before the first child of the parent node.
 * @function prependChild
 * @memberof domApi
 * @param {object} parentNode - HTML DOM Element Object.
 * @param {object} newNode - HTML DOM Element Object.
 */

/**
 * Removes a child node.
 * @function removeChild
 * @memberof domApi
 * @param {object} parentNode - HTML DOM Element Object.
 * @param {object} node - HTML DOM Element Object.
 */

/**
 * Adds a node to the end of the list of children of a the parent node.
 * @function appendChild
 * @memberof domApi
 * @param {object} parentNode - HTML DOM Element Object.
 * @param {object} newNode - HTML DOM Element Object.
 */

/**
 * Replaces one child node of the specified node with another.
 * @function replaceChild
 * @memberof domApi
 * @param {object} parentNode - HTML DOM Element Object.
 * @param {object} newNode - HTML DOM Element Object.
 * @param {object} oldNode - HTML DOM Element Object.
 */
const domApi = {
  insertBefore(parentNode, newNode, referenceNode) {
    parentNode.insertBefore(newNode, referenceNode);
  },
  nextSibling(node) {
    return node.nextSibling;
  },
  isInParent(parentNode, node) {
    return parentNode.contains(node) ? true : false;
  },
  insertAfter(parentNode, newNode, referenceNode) {
    parentNode.insertBefore(newNode, referenceNode.nextSibling);
  },
  prependChild(parentNode, newNode) {
    parentNode.insertBefore(newNode, parentNode.firstChild)
  },
  removeChild(parentNode, node) {
    parentNode.removeChild(node);
  },
  appendChild(parentNode, newNode){
    parentNode.appendChild(newNode);
  },
  replaceChild(parentNode, newNode, oldNode){
    parentNode.replaceChild(newNode, oldNode);
  }
};

export default domApi;
