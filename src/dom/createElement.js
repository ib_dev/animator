import setProps from './setProps';

/**
 * @module dom/createElement
 */

/**
 * Creates HTML DOM element nodes.
 * @function createElement
 * @param {object} node - Information about HTML DOM element node with its children nodes.
 */
function createElement(node) {
  let $el = null;
  if (node.type === 'textNode') {
    $el = document.createTextNode(node.content);
    node.el = $el;
    return $el;
  } else {
    $el = document.createElement(node.type);
    node.el = $el;

    if (node.props) setProps(node.el, node.props);

    node.children.map(vnode => {
      let el = null;
      if (vnode.type === 'textNode') {
        el = document.createTextNode(vnode.content);
        vnode.el = el;
      } else {
        el = createElement(vnode);
        vnode.el = el;
      }
      return el;
    }).forEach(node => $el.appendChild(node));
    return $el;
  }
}

export default createElement;
