/**
 * @module dom/addEvent
 */

/**
 * Add event listener with its arguments to HTML DOM element node.
 * @function addEvent
 * @param {object} $el - HTML DOM Element Object.
 * @param {string} name - HTML DOM event listener name.
 * @param {function|object} fooData - Function implementing the event listener interface
 * or array consisting of function implementing the event listener interface as first element and
 * this function arguments as other elements.
 */
const addEvent = ($el, name, fooData) => {
  const args = [];
  let foo = null,
      listener = null;
  if (Array.isArray(fooData)) {
    for (let i = 0; i < fooData.length; i++) {
      if (i === 0) {
        foo = fooData[i];
      } else {
        args.push(fooData[i]);
      }
    }
    listener = foo.bind(null, ...args);
  } else if (typeof fooData === 'function') {
    listener = fooData;
  }
  $el[name] = listener;
};

export default addEvent;
