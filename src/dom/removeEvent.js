/**
 * @module dom/removeEvent
 */

/**
 * Remove event listener from HTML DOM element node.
 * @function removeEvent
 * @param {object} $el - HTML DOM Element Object.
 * @param {string} name - HTML DOM event listener name.
 */
const removeEvent = ($el, name) => { 
  $el[name] = null;
};

export default removeEvent;
