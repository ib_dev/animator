import removeEvent from './removeEvent';

/**
 * @module dom/rmProp
 */


/**
 * Remove attribute or event listener from HTML DOM element node.
 * @function rmProp
 * @param {object} $el - HTML DOM Element Object.
 * @param {string} name - HTML DOM Attribute name.
 */
const rmProp = ($el, name) => {
  if (name === 'className') {
    $el.removeAttribute('class');
  } else if (/^on/.test(name)) {
    removeEvent($el, name);
  } else {
    $el.removeAttribute(name);
  }
};

export default rmProp;
