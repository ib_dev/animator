import setProp from './setProp';

/**
 * @module dom/setProps
 */

/**
 * Add attributes and event listeners to HTML DOM element node.
 * @function setProps
 * @param {object} $el - HTML DOM Element Object.
 * @param {object} props - Information needed to create HTML DOM attribute nodes
 * and event listeners for element node.
 */
const setProps = ($el, props) => {
  const propNames = Object.keys(props);
  for (let prop of propNames) {
    setProp($el, prop, props[prop]);
  }
};

export default setProps;
