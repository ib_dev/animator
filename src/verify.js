/**
 * @module verify
 */

/** Class for validation. */
class Verify {

  /**
   * Create pseudo-properties.
   * @param {object} arr - Array of pseudo-property names which will be used in validation.
   * @param {function} validation - Validation rules.
   */
  constructor(arr, validation) {
    this.validator = this.proxy(validation);
    arr.forEach(method => {
      Object.defineProperty(Verify.prototype, method, {
        get: ()=> {
          return this['p' + method];
        },
        set: val => {
          this['p' + method] = this.validator(method, val);
        }
      });
    })
  }

  /**
   * Checking if pseudo-property value is valid.
   * @param {function} handler - Validation rules.
   * @returns {function} Method for validation.
   */
  proxy(handler) {
    return (method, val) => {
      if (handler(method, val)){
        return val;
      } else {
        throw new Error('Validation is invalid');
      }
    };
  }
}

export default Verify;
