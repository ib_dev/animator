import { h } from './../../../src/';

const AnimationSettings = ({settingsUpdate, animation}) => {

  const { reorderDuration, order, enabled, add, remove } = animation;

  const orderMap = {
    dra: 'Delete -> Reorder -> Add',
    dar: 'Delete -> Add -> Reorder',
    rda: 'Reorder -> Delete -> Add',
    rad: 'Reorder -> Add -> Delete',
    adr: 'Add -> Delete -> Reorder',
    ard: 'Add -> Reorder -> Delete'
  }

  const validateNumber = e => e.charCode >= 48

  const handleUpdate = (type, e) => {
    const value = e.target.value;
    const data = (value && Number.isNaN(Number(value))) ? value : Number(value);
    settingsUpdate({[type]: data})
  }

  return h('div', {class: 'settings-wrapper'}, [
    h('h3', {class: 'settings-header'}, 'Animation settings for this list (not affecting other lists)'),

    h('div', {class: 'ui segment'}, [
      h('input', {
        oninput: [handleUpdate, 'reorderDuration'],
        onkeypress: validateNumber,
        onpaste: validateNumber,
        value: reorderDuration,
        class: 'input-number',
        type: 'number',
        step: 100,
        min: 0
      }),
      h('span', {class: 'setting-description'}, 'Reorder duration in milliseconds ')
    ]),

    h('div', {class: 'ui segment'}, [
      h('select', {class: 'select-box', onchange: [handleUpdate, 'order']},
        Object.keys(orderMap).map(value =>
          h('option', value === order ? {value, selected: true} : {value}, orderMap[value]))
      ),
      h('span', {class: 'setting-description'}, 'Animations order ')
    ]),

    h('div', {class: 'ui segment'}, [
      h('select', {class: 'select-box', onchange: [handleUpdate, 'enabled']}, [
        {value: 1, name: 'Enabled'},
        {value: 0, name: 'Disabled'}].map(({value, name}) =>
          h('option', enabled === value ? {value, selected: true} : {value}, name))
      ),
      h('span', {class: 'setting-description'}, 'Enable or disable ')
    ]),

    h('div', {class: 'ui segment'}, [
      h('select', {class: 'select-box', onchange: [handleUpdate, 'add']}, [
        'fade-in', 'fade-in-down', 'bounce-in-left', 'light-speed-in', 'rotate-in']
        .map(value => h('option', add === value ? {value, selected: true} : {value}, value))
      ),
      h('span', {class: 'setting-description'}, 'Class name for adding elements (CSS3 animation) ')
    ]),

    h('div', {class: 'ui segment'}, [
      h('select', {class: 'select-box', onchange: [handleUpdate, 'remove']}, [
        'fade-out', 'bounce-out-left', 'tricky-bounce-out-left', 'rotate-out']
        .map(value => h('option', remove === value ? {value, selected: true} : {value}, value))
      ),
      h('span', {class: 'setting-description'}, 'Class name for elements removal (CSS3 animation) ')
    ])

  ])
}

export default AnimationSettings;
