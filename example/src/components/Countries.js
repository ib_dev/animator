import store from './../store';
import { h } from './../../../src/';
import { withState, arrayToObject } from './../helpers';
import api from './../api';
import {
  COUNTRIES_BY_NAME_ADDED,
  COUNTRIES_BY_NAME_FETCH_FAILED,
  COUNTRIES_BY_NAME_VALIDATION_FAILED
} from './../types';
import InlineError from './InlineError';

const Countries = ({search, countries, error, animation, toggle}) => {

  let timer = 0;

  const handleFetch = e => {
    const val = e.target.value.trim();
    if (val === search) return false;
    clearTimeout(timer)

    if (!val) {
      store.dispatch({
        type: COUNTRIES_BY_NAME_VALIDATION_FAILED,
        error: "Can't be blank"
      })
    } else {
      timer = setTimeout(getCountries.bind(null, val), 200)
    }

  }

  const getCountries = search => {
    api.getCountriesByName(search)
      .then(res => {
        const countries = arrayToObject(res, 'cca3')
        store.dispatch({ type: COUNTRIES_BY_NAME_ADDED, countries, search })
      })
      .catch(err => {
        if (err.message === '404') {
          store.dispatch({
            type: COUNTRIES_BY_NAME_FETCH_FAILED,
            error: 'No search results',
            search
          })
        }
      })
  }

  return h('div', {}, [
    h('div', {}, [
      h('div', {class: 'ui stacked segment clearfix search-wrapper'}, [
        h('div', {class: 'f-left'}, [
          h('input', {
            class: 'input' + (error && ' input-error'),
            placeholder: 'Search countries',
            type: 'text',
            value: search,
            oninput: handleFetch
          }),
          error && InlineError(error)
        ]),
        h('i', {onclick: toggle, class: 'large cogs icon settings-icon f-right'})
      ]),
      h('div', {class: 'ui list countries-list', animation}, countries.length ? countries.map(item =>
        h('div', {class: 'item', key: 'a' + item.cca3}, [
          h('strong', {}, item.name.common),
          ', ' + item.capital[0]
        ])
      ) : [h('div', {key: 'a231d4245'})] )
    ])
  ])
}

const map = state => {
  return {
    search: state.countriesByName.search,
    countries: Object.values(state.countriesByName.countries)
      .filter((item, i) => i <= 20 ? item : false), 
    error: state.countriesByName.error
  }
}

export default withState(store, map)(Countries);
