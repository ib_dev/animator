import { h } from './../../../src/';

const InlineError = (error = '') =>
  h('span', {className: 'error-message'}, error)

export default InlineError;
