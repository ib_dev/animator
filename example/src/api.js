
const status = res => {
  if (!res.ok) {
    throw new Error(res.status);
  }
  return res;
}

export default {
  getCountriesByName: name =>
    fetch(`https://restcountries.com/v3.1/name/${name}?fields=name,capital,cca3`)
    .then(status).then(res => res.json())
}
