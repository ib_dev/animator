import { Animator } from './../../src/';

export const render = (Component, $root) => store => {
  const animator = new Animator($root);
  animator.render = Component()
  store.subscribe(() => {
    animator.render = Component()
  })
}

export const withState = (store, map) => Component => (props = {}) =>
  Component(Object.assign({}, props, map(store.getState())))


export const arrayToObject = (arr, key) =>
  arr.reduce((obj, item) => {
    obj[item[key]] = item
    return obj
  }, {})
