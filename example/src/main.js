// polyfills
import "babel-polyfill";
import 'whatwg-fetch'

import store from './store';
import { render } from './helpers';
import App from './App';


window.addEventListener('DOMContentLoaded', () => {
  render(App, document.getElementById('app'))(store)
});
