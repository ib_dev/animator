import { combineReducers } from 'redux'
import countriesByName from './reducers/countriesByName';

const reducers = combineReducers({
  countriesByName
})

export default reducers;
