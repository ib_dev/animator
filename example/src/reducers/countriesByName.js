import {
  COUNTRIES_BY_NAME_ADDED,
  COUNTRIES_BY_NAME_FETCH_FAILED,
  COUNTRIES_BY_NAME_VALIDATION_FAILED,
  COUNTRIES_BY_NAME_ANIMATION_SETTINGS_CHANGED,
  TOGGLE_VIEW
} from './../types';

const initialAnimation = {
  enabled: true,
  remove: 'tricky-bounce-out-left',
  add: 'fade-in-down',
  reorderDuration: 1200,
  order: 'rda'
}

const initialState = {
  countries: {},
  search: '',
  error: '',
  animation: initialAnimation,
  countriesVisible: true
}

export default function(state = initialState, action) {
  switch (action.type) {
  case COUNTRIES_BY_NAME_ADDED:
    return Object.assign({}, state, {
      countries: action.countries,
      search: action.search,
      error: ''
    })
  case COUNTRIES_BY_NAME_FETCH_FAILED:
    return Object.assign({}, state, {
      error: action.error,
      search: action.search,
      countries: {}
    })
  case COUNTRIES_BY_NAME_VALIDATION_FAILED:
    return Object.assign({}, state, {
      error: action.error,
      search: action.search,
      countries: {}
    })
  case COUNTRIES_BY_NAME_ANIMATION_SETTINGS_CHANGED:
    return Object.assign({}, state, {
      animation: Object.assign({}, state.animation, action.settings)
    })
  case TOGGLE_VIEW:
    return Object.assign({}, state, {
      countriesVisible: !state.countriesVisible,
      countries: {}, search: ''
    })
  default:
    return state
  }
}
