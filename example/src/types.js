export const COUNTRIES_BY_NAME_ADDED = "COUNTRIES_BY_NAME_ADDED";
export const COUNTRIES_BY_NAME_FETCH_FAILED = "COUNTRIES_BY_NAME_FETCH_FAILED";
export const COUNTRIES_BY_NAME_VALIDATION_FAILED = "COUNTRIES_BY_NAME_VALIDATION_FAILED";
export const COUNTRIES_BY_NAME_ANIMATION_SETTINGS_CHANGED = "COUNTRIES_BY_NAME_ANIMATION_SETTINGS_CHANGED";
export const TOGGLE_VIEW = "TOGGLE_VIEW";
