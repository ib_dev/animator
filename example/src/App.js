import { h } from './../../src/';
import { withState } from './helpers';
import store from './store';
import {
  COUNTRIES_BY_NAME_ANIMATION_SETTINGS_CHANGED,
  COUNTRIES_BY_NAME_ADDED,
  TOGGLE_VIEW
} from './types';
import Countries from './components/Countries';
import AnimationSettings from './components/AnimationSettings';

const App = ({animation, countriesVisible}) => {

  let timer = 0;

  const settingsUpdate = settings => {
    clearTimeout(timer)
    timer = setTimeout(() => {
      store.dispatch({
        type: COUNTRIES_BY_NAME_ANIMATION_SETTINGS_CHANGED, settings
      })
    }, 500)
  }

  const toggle = () => {
    store.dispatch({ type: TOGGLE_VIEW })
  }

  return h('div', {class: 'ui container app-container'}, [
    h('div', {className: 'ui two column centered grid'}, [
      countriesVisible ? h('div', {class: 'column'}, [
        Countries({animation, toggle})
      ]) : h('div', {class: 'column'}, [
        AnimationSettings({settingsUpdate, animation}),
        h('button', {onclick: toggle, class: 'ui button'}, 'Try new settings')
      ])
    ])
  ])
}

const map = state => {
  return {
    animation: state.countriesByName.animation,
    countriesVisible: state.countriesByName.countriesVisible
  }
}

export default withState(store, map)(App);
