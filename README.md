## Animator - JavaScript Virtual DOM library with animation capabilities ##

JavaScript Virtual DOM library. Animations (CSS3) for lists with keys (key is used to uniquely identify list item). 
Items reordering, removal and deleting can be animated separately with different animations.

## Test it ##

[Demo](https://animator-vdom-demo.web.app/)

## The most important technologies used ##

ECMAScript 6 + Redux used only in example. 

## Setup ##

Run `pnpm dev` to run the app in development mode.   
Run `pnpm build` to build the app for production.   
Run `pnpm preview` to run a local server.
